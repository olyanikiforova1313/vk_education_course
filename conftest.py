import sys


def pytest_addoption(parser):  # позволяет считывать значения переменных
    parser.addoption('--env', type=str, default='prod')


# Проверяет в один поток запуск или нет
def is_master(config):
    if hasattr(config, 'workerinput'):
        return False
    return True


# можно определить на каком потоке был запуск хука. На основном (master) или на gw.
# это хук. Действия выполняемые при запуске тестов. При многопоточном запуске.
def pytest_configure(config):
    if is_master(config):
        print('This is configure hook on MASTER\n')
    else:
        sys.stderr.write(f'This is configure hook on WORKER {config.workerinput["workerid"]}\n')


# Действия выполняемые при завершении тестов
def pytest_unconfigure(config):
    if is_master(config):
        print('This is unconfigure hook on MASTER\n')
    else:
        sys.stderr.write(f'This is unconfigure hook on WORKER {config.workerinput["workerid"]}\n')
