import pytest


def test1():
    assert 1 == 2

class Test:
    def test2(self):
        assert 1 != 2


def check_zero_division(a, b):
    assert a / b


def test_negative():
    with pytest.raises(ZeroDivisionError):
        check_zero_division(1, 0)
        pytest.fail(reason='No zeroDivisionError error occured')
