import pytest

# def test():
#     for i in range(10):
#         assert i % 2 == 0
#
#
# def test_even():
#     errors = []
#     for i in range(10):
#         try:
#             assert i % 2 == 0
#
#         except AssertionError:
#             errors.append(f'{i} not even')
#     assert not errors

# Тест будет выполняться с каждым параметром. Т.е. получится 10 независимых тестов
@pytest.mark.parametrize('i', list(range(10)))
def test_even2(i):
    assert i % 2 == 0