import os

import pytest
import random


@pytest.fixture()
def random_value():
    """
    Сделали этот метод фикстурой и теперь его можно передать в тест
    """
    print("entering")
    yield random.randint(0, 100)
    print("exiting")


def test(random_value):
    """
    Теперь pytest понимает, что есть фикстура и ее надо вызвать при запуске теста
    """
    assert random_value > 50


@pytest.fixture(scope="function")
def func_fixture():
    yield random.randint(0, 100)


@pytest.fixture(scope="class")
def class_fixture():
    yield random.randint(0, 100)


@pytest.fixture(scope="session")
def session_fixture():
    yield random.randint(0, 100)


class Test:
    def test1(self, func_fixture, class_fixture, session_fixture):
        print(func_fixture,class_fixture, session_fixture)

    def test2(self, func_fixture, class_fixture, session_fixture):
        print(func_fixture, class_fixture, session_fixture)


@pytest.fixture(autouse=True)
def new_file(random_file):
    """
    Этот метод позволит на каждый тест создавать файл, а в конце его удалять
    """
    f = open(random_file, "w")
    yield
    f.close()
    os.remove(random_file)

@pytest.fixture()
def random_file():
    # возвращает результат в фикстуру new_file
    return str(random.randint(0,1000))